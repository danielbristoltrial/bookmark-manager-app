export interface Bookmark {
	
	id: number;
	title: string;
	url: string;
	group: string;
}