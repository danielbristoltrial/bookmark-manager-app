import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { OverlayModule } from '@angular/cdk/overlay';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { initialState, reducers, effects } from './store/state/app.state';

import { FlexLayoutModule } from '@angular/flex-layout';

import { GroupComponent } from './components/group/group.component';
import { PopupComponent } from './components/popup/popup.component';
import { MessageComponent } from './components/popup/popup.component';
import { SimpleTableComponent } from './components/simple-table/simple-table.component';
import { AddGroupComponent } from './components/add-group/add-group.component';
import { UpdateGroupComponent } from './components/update-group/update-group.component';

import { BookmarkService } from './services/bookmark.service';
import { NotificationService } from './services/notification.service';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
	    MatButtonModule,
	    MatDialogModule,
	    MatIconModule,
	    MatInputModule,
	    MatSidenavModule,
	    MatSnackBarModule,
	    MatSortModule,
	    MatTableModule,
	    MatToolbarModule,
	    MatTooltipModule,
	    OverlayModule,
        FormsModule,
        FlexLayoutModule,
        EffectsModule.forRoot(effects),
        StoreModule.forRoot(reducers, {initialState}),
        StoreDevtoolsModule.instrument( {maxAge: 30} ),
    ],
    declarations: [
        AppComponent,
        GroupComponent,
        PopupComponent,
        MessageComponent,
        SimpleTableComponent,
        AddGroupComponent,
        UpdateGroupComponent,
        HeaderComponent
    ],
    providers: [
	    BookmarkService, NotificationService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }