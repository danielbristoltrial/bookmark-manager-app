import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AddGroupComponent } from '../add-group/add-group.component';
import { UpdateGroupComponent } from '../update-group/update-group.component';
import { AppState, selectBookmarks, selectGroup, selectBookmarksByGroup } from '../../store/state/app.state'; 
import { Bookmark } from '../../store/state/bookmark.state';
import { InitialBookmark, DeleteBookmark } from '../../store/actions/bookmark.actions';
import { NotificationService } from '../../services/notification.service';

@Component({
    selector: 'group',
    styleUrls: ['group.component.scss'],
    templateUrl: 'group.component.html',
})
export class GroupComponent {
    groupTableConfig;
    addPopupConfig;
    activeGroup;
    groups$: Observable<string[]>;
    groups: string[];
    bookmarks$: Observable<Bookmark[]>;
    bookmarks: Bookmark[];
    selectedGroup: string;

    constructor(private store: Store<AppState>) {

    }

	ngOnInit() {
		console.log("this: " + this.bookmarks)
	    this.addPopupConfig = {
			type: 'template',
			attribute: 'mat-mini-fab',
			fontIconColor: 'primary',
			fontIcon:'add',
			tooltip:'Add Group',
			template: AddGroupComponent
		}

		this.loadData();
		
		this.groupTableConfig = {
			data: this.bookmarks,
			displayedColumns: ['title', 'url', 'action'],
			columns: [{
				header: "Title",
				field: "title"
			},{
				header: "Url",
				field: "url"
			}],
			editPopupConfig: {
				type: 'template',
				attribute: 'mat-icon-button',
				fontIconColor: 'primary',
				fontIcon:'edit',
				tooltip:'Edit Group',
				template: UpdateGroupComponent
			},
			deletePopupConfig: {
				type: 'message',
				attribute: 'mat-icon-button',
				fontIconColor: 'warn',
				fontIcon:'delete',
				tooltip:'Delete Group',
				messageConfig: {
				    message: 'Are you sure you want to delete this bookmark?',
                    primaryButtonEnabled: true,
                    primaryButtonLabel: 'Yes',
                    secondaryButtonEnabled: true,
                    secondaryButtonLabel: 'No',
                    primaryButtonCallback: function(store: Store<AppState>, notificationService: NotificationService, data: any) {
	                    store.dispatch(new DeleteBookmark(data));
					    notificationService.showNotification({
						    duration: 2000,
						    vPos: 'top',
						    hPos: 'center',
						    message: 'Bookmark successfully deleted.'
					    });
                    }
				}
			}
		}
	}

	isActive(group: string) {
	    return group === this.activeGroup;
	}

    loadData() {
	    this.groups$ = this.store.pipe(select(selectGroup));
        this.groups$.subscribe(response => {
		    this.groups=response;
        });

        this.loadGroup('All Bookmarks');
        this.activeGroup = 'All Bookmarks';
        this.loadBookmarks();
    }

    loadGroup(group : string) {
	    this.selectedGroup = group;
        if(group === 'All Bookmarks') {
	        this.bookmarks$ = this.store.pipe(select(selectBookmarks));
        } else {
	        this.bookmarks$ = this.store.pipe(select(selectBookmarksByGroup(group)));
        }

        this.bookmarks$.subscribe(response => {
		    this.bookmarks=response;

			if(this.groupTableConfig) {
				this.groupTableConfig.data=this.bookmarks;
			}
        });

        this.activeGroup = group;
    }

    private loadBookmarks() {
	    this.store.dispatch(new InitialBookmark(null));
    }
}
