import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/state/app.state';
import { NotificationService } from '../../services/notification.service';

interface SimpleTableConfig {
	columns: any[]
	displayedColumns: string[]
    tooltip: any
    data: any
    editPopupConfig: any
    deletePopupConfig: any
    deleteFunc: Function
}

@Component({
	selector: 'simple-table',
    styleUrls: ['simple-table.component.scss'],
    templateUrl: 'simple-table.component.html',
})
export class SimpleTableComponent {
	
    constructor(private store: Store<AppState>, private notificationService: NotificationService) {
        
    }

    @Input() config : SimpleTableConfig;

	delete(data: any) {
		this.config.deleteFunc(data, this.store, this.notificationService);
	}

}
