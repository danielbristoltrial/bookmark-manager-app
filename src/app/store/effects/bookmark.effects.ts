import { Actions, ofType, Effect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import {switchMap, map} from 'rxjs/operators';
import { BookmarkActionTypes, InitialBookmark, LoadBookmarkDone } from '../actions/bookmark.actions';
import { BookmarkService } from '../../services/bookmark.service';

@Injectable({providedIn: 'root'})
export class BookmarkEffects {

    constructor(private actions$: Actions, private bookmarkService: BookmarkService){ }

    @Effect()
    public loadBookmarks$ = this.actions$.pipe(
        ofType(BookmarkActionTypes.INITIAL_BOOKMARK),
        switchMap((action: InitialBookmark) =>
            this.bookmarkService.getBookmarks().pipe(
                map(bookmarks => new LoadBookmarkDone(bookmarks))
            )
        )
    );
}
