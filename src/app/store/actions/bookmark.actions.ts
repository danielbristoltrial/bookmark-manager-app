import { Action } from '@ngrx/store';

import { Bookmark, Bookmarks } from '../state/bookmark.state';

export enum BookmarkActionTypes {
    GET_BOOKMARK = '[Bookmark] Get Songs',
    ADD_BOOKMARK = '[Bookmark] Add Song',
    UPDATE_BOOKMARK = '[Bookmark] Edit Song',
    DELETE_BOOKMARK = '[Bookmark] Delete Song',
    INITIAL_BOOKMARK = '[Bookmark] Load initial state',
	LOAD_BOOKMARK_DONE = '[Bookmark] Load bookmark done',
	RESTORE_BOOKMARKS = '[Bookmark] Restore bookmarks'
}

export class ActionParent implements Action {
    type: any;
	payload: any;
}

export class GetBookmarks implements ActionParent {
    readonly type = BookmarkActionTypes.GET_BOOKMARK;

    constructor(public payload: any) {
        console.log('ACTION ' + BookmarkActionTypes.GET_BOOKMARK);
    }
}

export class AddBookmark implements ActionParent {
	readonly type = BookmarkActionTypes.ADD_BOOKMARK
	constructor(public payload: any) {
        console.log('ACTION ' + BookmarkActionTypes.GET_BOOKMARK);
    }
}

export class UpdateBookmark implements ActionParent {
  readonly type = BookmarkActionTypes.UPDATE_BOOKMARK;

  constructor(public payload: Bookmark) {
      console.log('ACTION ' + BookmarkActionTypes.UPDATE_BOOKMARK);
  }
}

export class DeleteBookmark implements ActionParent {
    readonly type = BookmarkActionTypes.DELETE_BOOKMARK;

    constructor(public payload: Bookmark) {
        console.log('ACTION ' + BookmarkActionTypes.DELETE_BOOKMARK);
    }
}

export class InitialBookmark implements ActionParent {
    readonly type = BookmarkActionTypes.INITIAL_BOOKMARK;

    constructor(public payload: any){
        console.log('ACTION ' + BookmarkActionTypes.INITIAL_BOOKMARK);
    }
}

export class LoadBookmarkDone implements Action {
    readonly type = BookmarkActionTypes.LOAD_BOOKMARK_DONE;

    constructor(public payload: Bookmarks) {
        console.log('Songs - ' + payload);
    }
}

export class RestoreBookmarks implements Action {
  readonly type = BookmarkActionTypes.RESTORE_BOOKMARKS;

  constructor(public payload: any) {
      console.log('ACTION ' + BookmarkActionTypes.RESTORE_BOOKMARKS);
  }
}

export type BookmarkActions = GetBookmarks | AddBookmark | UpdateBookmark | DeleteBookmark | InitialBookmark | LoadBookmarkDone | RestoreBookmarks;
