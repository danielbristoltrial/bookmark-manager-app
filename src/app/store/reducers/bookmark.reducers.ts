import { BookmarkActionTypes, BookmarkActions } from '../actions/bookmark.actions';
import { Bookmarks, InitialBookmarkState } from '../state/bookmark.state';

export function bookmarkReducer(state = InitialBookmarkState, action: BookmarkActions): Bookmarks {
	switch (action.type) {
        case BookmarkActionTypes.GET_BOOKMARK: {
            return  {...state};
        }
        case BookmarkActionTypes.ADD_BOOKMARK: {
            return  { bookmarks: sort(state.bookmarks.concat(action.payload), 'name')};
        }
        case BookmarkActionTypes.UPDATE_BOOKMARK: {
	        return  { bookmarks: sort(state.bookmarks.filter(bookmark => bookmark.id !== action.payload.id).concat(action.payload), 'name')};
        }
        case BookmarkActionTypes.DELETE_BOOKMARK: {
            return  { bookmarks: sort(state.bookmarks.filter(bookmark => bookmark.id !== action.payload.id), 'name')};
        }
		case BookmarkActionTypes.LOAD_BOOKMARK_DONE: {
            return  { bookmarks: sort(state.bookmarks.concat(action.payload.bookmarks), 'name')};
        }
        default: {
            return {
                ...state
            };
        }
	}
}

function sort(bookmarks, property) {
	return bookmarks.sort((a,b) => {
        if(a[property] < b[property]) {
            return -1;
        } else {
            return (a[property] > b[property]) ? 1 : 0
        }
    });
}