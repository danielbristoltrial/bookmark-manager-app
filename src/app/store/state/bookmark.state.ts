export interface Bookmark {
	id: number,
	title: string,
	url: string,
	group: string
}

export interface Bookmarks {
	bookmarks: Bookmark[];
}

export const InitialBookmarkState: Bookmarks = {
	bookmarks: []
}