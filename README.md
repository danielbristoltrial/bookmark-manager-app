# Bookmark Manager #
Show, Create, update, and delete bookmarks

## Getting Started ##
1. Clone this repository on your local machine
2. Open terminal
3. Go to the project directory
4. Type npm install - This will install all the dependencies of the project
4. Type npm start - This will open up the application on the browser

### Prerequisites ###
NodeJS version 13.9.0
NPM Version 6.13.7

## Home Screen##
![HomePage](bookmark-images/All.PNG)

## Add Bookmark ##
![Add](bookmark-images/Add.PNG)

## Update Bookmark ##
![Update](bookmark-images/Update.PNG)

## Delete Bookmark ##
![Delete](bookmark-images/Delete.PNG)

## Select Group ##
![Group](bookmark-images/Group.PNG)
